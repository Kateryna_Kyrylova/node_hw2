const { Note } = require('./models/Notes');

function createNote(req, res) {
  const { text, userId } = req.body;
  const note = new Note({
    text,
    userId,
  });
  note.save()
    .then((saved) => {
      res.json(saved);
    });
}

function getNotes(req, res) {
  return Note.find().then((result) => {
    res.json(result);
  });
}

const getNote = (req, res) => Note.findById(req.params.id)
  .then((note) => {
    res.json(note);
  });

const updateNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  const { text } = req.body;

  if (text) note.text = text;

  return note.save().then((saved) => res.json(saved));
};

const deleteNote = (req, res) => Note.findByIdAndDelete(req.params.id)
  .then((note) => {
    res.json(note);
  });

const getMyNotes = (req, res) => Note.find({ userId: req.user.userId }, '-__v').then((result) => {
  res.json(result);
});

const updateMyNoteById = (req, res) => {
  const { text } = req.body;
  return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
    .then(() => {
      res.json({ message: 'Note was updated' });
    });
};

const markMyNoteCompletedById = (req, res) => Note.findByIdAndUpdate(
  { _id: req.params.id, userId: req.user.userId },
  { $set: { completed: true } },
).then(() => {
  res.json({ message: 'Note was marked completed' });
});

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
  getMyNotes,
  updateMyNoteById,
  markMyNoteCompletedById,
};
