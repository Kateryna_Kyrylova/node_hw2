const express = require('express');

const router = express.Router();
const {
  createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
  getMyNotes,
  updateMyNoteById,
  markMyNoteCompletedById,
} = require('./notesService');

const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', createNote);

router.get('/', authMiddleware, getNotes);

router.get('/get-my-notes', authMiddleware, getMyNotes);

router.get('/:id', authMiddleware, getNote);

router.patch('/update-my-note/:id', authMiddleware, updateMyNoteById);

router.patch('/mark-completed/:id', authMiddleware, markMyNoteCompletedById);

router.patch('/:id', updateNote);

router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
