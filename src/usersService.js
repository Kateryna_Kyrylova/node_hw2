const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const registerUser = async (req, res, next) => {
  const { name, username, password } = req.body;

  const user = new User({
    name,
    username,
    password: await bcrypt.hash(password, 10),
  });

  user.save()
    .then((saved) => res.json(saved))
    .catch((err) => {
      next(err);
    });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    // eslint-disable-next-line no-underscore-dangle
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.json({ message: 'Success', token: jwtToken });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
